//Declare Dependencies

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//Define your schema

const teamSchema = new Schema(
	{
		name:String
	},
	{
		timestamps: true
	}
	);

//Export your model

module.exports = mongoose.model("Team", teamSchema);