//declare dependencies

const express = require("express");
const app = express();
const mongoose = require("mongoose");

//connect to DB
mongoose.connect("mongodb://localhost:27017/mern_tracker", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once("open", () => {
	console.log("Now connected to local MongoDB");
});

//Apply Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//Declare Model
const Team = require("./models/teams");
const Member = require("./models/members");
const Task = require("./models/tasks")

//Create Routes/Endpoints
//1) CREATE
app.post("/teams", (req, res) =>{
	// return res.send(req.body);
	const team = new Team(req.body);
	//save to DB
	team.save()
	.then(() => {res.send(team)})
	.catch((e) => {res.status(400).send(e)}) //BAD REQUEST (http response status codes)
});

//2)GET ALL
app.get("/teams", (req, res)=>{
	// return res.send("get all teams");
	Team.find().then((teams) => { return res.status(200).send(teams)})
	.catch((e) => { return res.status(500).send(e)})
})

//3)GET ONE
app.get("/teams/:id", (req, res) =>{
	// return res.send("get a team");
	// console.log(req.params.id)
	const _id = req.params.id;

	//Mongose Models Query
	Team.findById(_id).then((team) => {if(!team){
		//NOT Found
		return res.status(404).send(e)
	} return res.send(team)

	})
	.catch((e) => {return res.status(500).send(e)})
})

//4)UPDATE ONE
app.patch("/teams/:id", (req, res) =>{
	// return res.send("update a team");
	const _id = req.params.id

	Team.findByIdAndUpdate(_id, req.body, {new:true}).then((team) => {
		if(!team){return res.status(404).send(e)}
		return res.send(team)
	})
	.catch((e) => {return res.status(500).send(e)})
})

//5)DELETE ONE
app.delete("/teams/:id", (req, res)=> {
	// return res.send("delete a team");

	const _id = req.params.id;

	Team.findByIdAndDelete(_id).then((team) => {if(!team){return res.status(404).send(e)}
		return res.send(team)
	})
	.catch((e) => {return res.status(500).send(e)})
})
	

//initialize the server

app.listen(4000, () => {
	console.log("Now listening to port 4000");
});